# Font Dorothy Matrix

_Dorothy Matrix_ (also known by the nickname "Dot" Matrix) is
a font family created from
the aesthetics of 1980’s dot matrix printers.
In fact the form of the core set of letterforms
is taken programmatically from
the dot maps of the ROM font of the Epson FX-80 9-pin dot matrix
printer.

Compared to other dot-based fonts of letters, _Dorothy_
distinguishes itself by:

- using the authentic matrix layout
- including the horizontal half-step feature (the grid is not a
  simple rectangular layout, as the printer incorporated a
  half-step in its designs; see the left-hand bowl of the `/a`
  or the foot on its right)
- the additional weight (strictly a Demi-Bold) based
  on the existing Emphasized mode of the FX-80

The principal member of the family is Dorothy, but
there are also other family members;
cousins and so on.
_Dorothy Wren_ is built on the same body as Dorothy (5-x, 7-Cap, 9-Em),
but with the design taken from printouts of a wide carriage
dot matrix printer.
_Norma_, a slightly more distant relative, has a taller matrix,
being built with a 7-dot x-height, 10-dot Cap-height, and
13-dot Em.
It is an original dot design, but based on
the TGL 31034 lettering standard.

In Dorothy, the glyph repertoire has been extended somewhat
from the ROM font to cover Welsh and Slovenian.
Arrows have been added, and
a few whimsy characters such as U+1F950 CROISSANT and
[dozenal symbols (base 12)](https://en.wikipedia.org/wiki/Duodecimal).

<img src="image/DorothyMatrixMLAB20240515-Regular-plaque.png" />

Other members of the family have more restricted repertoires.

The dot matrix printers of the 1980’s were largely monospaced,
and so most of the family is monospaced too,
indicated by appending "M",
making Dorothy's full name _Dorothy Matrix M_.

Norma is a proportional design, with a "P" suffix.
The FX-80 also supported a proportional mode which
used the same letterforms but with proportional spacing;
that mode isn't realised in a font, but it
suggests a possible direction for future work.

For Dorothy and Wren,
the dotted forms are created from the 9 pins of the hypothetical
printer.
These 9 pins are stacked vertically and progress horizontally to
strike out the required shapes in a form of impact printing.
Although there are 9 pins, only 8 can be used for a given
glyph (either the upper 8, for ascenders, or the lower 8, for
descenders) which constrains the design somewhat.
Both Dorothy and Dorothy Wren have a basic design
that uses a reasonably robust and pleasing 7-dot Cap-height,
with 5-dot x-height, and 2 dots below for descenders.

The original ROM font includes a smattering of accented letters,
which for the lowercase vary between acceptable and problematic.
For the uppercase the accented forms are mostly very problematic
because the 7 available dots means that the capitals have to duck down
into 5-dot-high shapes leaving a space 2-dot-high for the
accents, which is not really enough.

Where i have extended the repertoire with additional letters and
accents, i have continued this constraint, leading to some
authentically questionable designs.

If you think the **i** is not centred and is a little too far to
the left, i agree, but it is authentic to the original ROM.

Norma is based on a larger matrix.
Cap-height 10, x-height 7, 3 dots for descenders.
And also 3 dots above the Cap-line for diacritics;
limited to the German umlauts in this design.


## Repository Layout

This repo is intended to follow the
[guidelines for upstream repos for Google Fonts](https://googlefonts.github.io/gf-guide/upstream.html).


## Building the fonts

The fonts are currently built with extremely non-conventional
tooling, using custom Python scripts to make the SVG and CSV
source that the Font 8 tools consume and convert to TTF font
files.

For CI, an automation of this process is available in
`.gitlab-ci.yml`.


### Requirements

- git, make (almost universal)
- Go, Python 3 (commonly available)
- Font 8 (not commonly available: `git clone https://git.sr.ht/~drj/font8; cd font8; sh mk.sh` and now put
  blahblabblah`/font8/build` on your `$PATH`)

### Run the build script

clone _this_ repo, cd into it, and run `sources/build.sh`

    git clone https://gitlab.com/drj11/font-dorothy
    cd font-dorothy
    sh sources/build.sh

Artefacts, font files, are in `/fonts`...

### Bonus builds

The above `sh sources/build.sh` builds a default selection of
the fonts in the Dorothy font family.

A "full run" is

    sh sources/build.sh --members dorothy,wren,norma


## Working notes

The ROM font contains a bunch of glyphs.
Each glyph has:

- its ROM index (0 to 255 are defined by OEM ROM)
- family membership (128 to 255 are italic)
- pin height (9 for the FX-80)
- width in _bits_
- nominal grid (Origin at [0.5 0.5], step by [0.5 0] and [0 1])
- width in nominal pin-units (Epson 9-pin is 1 point
  centre-on-centre)

ROM glyphs present as a series of _bits_ (0 or 1 in a
rectilinear grid) _and_ in various realisations as a series of
_dots_ (positioned at their nominal centres in an x–y plane).

The dot realisations are in this case an emulation of the Epson
9-pin printers.

I suggest that a procedure for converting the bit series into
dots is called a _Protocol_.

The Epson 9-pin printers supported various modes
([FX80UM1983] page xxiii):

- Pica, 10 CPI, standard pitch for typewriters
- Elite, 12 CPI, secondary standard pitch for typewriters
  (Olympia's Elite#87 was 12 CPI, but their Elite#8 was 11 CPI;
  according to https://www.mrmrsvintagetypewriters.com/blogs/news/typewriter-fonts)
- Compressed, 17.16 CPI per [FX80UM1983] page 40; see discussion
- Emphasized Pica, which i think is Pica struck again 0.5 CoC to
  the right
- Expanded Pica / Elite / Compressed / Emphasised Pica

Pica and Elite are sort of standard terms of art for
typewriters, and mostly corresponding to a particular pitch,
that is horizontal width.
Pica is also a term of art in typography and printing where it
corresponds to 12 points (which historically were not the exact
1/72 inch points we have today in the digital era).

These are all produced from the same bit patterns.
Pica, Elite, Compressed only vary in the horizontal nominal CoC
distance (1.2, 1.0, and 0.7 points).
Emphasized Pica is Pica with a copy of itself struck one half
CoC to the right (that is, imagine Pica striking each vertical
column twice, once in its original position and again after the
print head has moved 0.6 points to the right).

Expanded doubles the width of everything, effectively
converting each bit into two successive bits, at the bit level.

Double-strike (not otherwise documented here) strikes each pin
twice in the same location, putting more ink down for a denser
coverage.

Compressed is listed in the FX-80 manual as 17.16 CPI, meaning
that the character width is 4.196 points.
It's much more elegant to assume a deci-point based system with
720 dp per inch;
it makes the Pica/Elite/Compressed come out with nice round
numbers for the pin centre-to-centre:

- Pica 12 deci-points (1.2 points); character width 7.2 points
- Elite 10 deci-points (1 point); character width 6 points
- Compressed 7 deci-points (0.7 points); character width 4.2 points

If the Compressed character width really is 4.2 points then the
CPI is 17.14, which is different from the 17.16 quoted in the manual.
It's a tiny difference on the width of the character, and still
gives 137 characters across the 8 inch drawing area (137.28 for
the published pitch, 137.14 for the 42 deci-point pitch).

So here i am going to implement Compressed as having a pin
centre-to-centre of 7 deci-points;
which makes the 6-pin monospace character width 42 deci-points.

While this sounds quite elegant, and it is, the basic unit of
horizontal motion for these Epson 9-pin designs is the half-pin
step, which comes out as 3.5 deci-points for Compressed.
Expressed in 20ths of a point that would be 70/1440 inch (not
the first time the inch has been divided into 1440 units).


## Wide Carriage / Dorothy Wren

In 2024 i found some printouts from a wide-carriage dot matrix
printer, and used that to reconstruct its dot matrix.
It uses the same 9-pin print-head design, the same 7-high cap-height
and 5-high x-height, and the same horizontal half-step feature.

The /A apex is a single dot; 2 for Epson.

The /B is unusual (compared Epson and similar designs)
in having a middle horizontal stroke that does not meet the
vertical stroke, it has a half-step gap.

The /C is more rounded (/G as well, which has a very narrow
aperture).

For some letters (/I /D and so on), it is not clear what the
true left- and right- sidebearings should be, so they have
been estimated.

Occasionally there is a missing dot, or a dot leaving an
impression so faint as to be almost invisible.
Sometimes this is partly consistent. For example, on a page
that has several lines starting /M (for MOVE), the dot pattern
on the 3rd row should have two adjacent dots in the middle, but
a lot of them are missing the second dot.
Some electromechanical problem like a misfiring of the solenoid?

The /Q is rounder (than Epson), sharing a shape with /C and /G,
and not sharing the /O shape.

/asterisk has a cool design with a hole in the middle;
essentially the Epson design without the middle dot.

The /j appears to have no dot, and i've passed this as correct.
I reasoned that this printer probably has the same 8-row
restriction that the Epson FX-80 has: Of the 9 pins in the
print-head, only 8 (either the top 8 or the bottom 8) can be
used in any single glyph design (due to the convenience of 8-bit
coding).
This /j uses the bottom 2 rows for the descender, and therefore
would be unable to have a dot on the top row.
I think it's a poor design, better to do what the Epson does and
use a single row for the descender, which allows a dot to be
placed.

/q is synthesized, there are no examples in the printed
materials.

For /y it is a little hard to interpret the tail.


## ISO Norm / Dorothy Norma

Kerning progress:

- lowercase kerned
- Uppercase with lowercase kerned
- todo: Upper with upper

I made _Norma_, based on fragments of DIN, TGL, and ISO standards.

- 10 unit high cap-height;
- 7 unit high x-height
- narrow proportions.

Descenders match ascenders and are 3 units;
giving a 13 unit high body.
Capitals with diacritics, the german umlaut, use an additional 3
units above the Cap-height.

The punctuation displayed in the TGL specimen is fairly limited,
i have added a small number of extra punctuations and symbols.

Quotation marks as shown in the source images are low double
quotes (Unicode U+201E) DOUBLE LOW-9 QUOTATION MARK and regular
RIGHT DOUBLE QUOTATION MARK.
The low marks are clearly drawn _below_ the baseline;
this is unusual for modern designs, but i have kept it.
I've also added LEFT DOUBLE QUOTATION MARK as a clone of the
RIGHT (if you like, you can think of them as rotated, because
the actual drawing is just simple stroke).

Notes:

The constraints of the dot matrix, and typographic aesthetics,
sometimes forces a departure from the standard drawings.

Minor deviations:

- Throughout, rounded corners are modelled as a short diagonal section.
- waistline / arm in /B/E/F/G/H/R are all on row 6 (counting the
  row on the baseline as 1); this creates counters that are
  bigger in the bottom part of the letter;
- slope on /A /V /W /v /w /y is very unsatisfactory to model in dots,
  and for /V and the lower case, they have been redrawn with upper
  vertical strokes;
- On /N it's not possible to have a diagonal with a pleasing
  slope, and that correctly joins the vertical terminals; the
  compromise here is to make it slightly high-waisted;
- On /Q the diagonal stroke is made steeper and it projects less
  (to the right);
- The /S is symmetric instead of having a slightly larger lower
  counter;
- On /X /x the arms have kinks, in order to have better
  proportions overall;
- The /Z has a shorter top stroke, in order to make the diagonal
  stroke join nicely;
- On /r both the top corners are "square" whereas in the
  original drawing the NE corner is slightly rounded;
- On /t the right-hand portion of the crossbar is somewhat
  bigger than the left-hand portion; whereas in the original
  drawing the difference is much less;
- On /z the diagonal has an ogee that is more pronounced in Norma
  than the original drawing;
- The /zero of Norma has sharper "corners",
  in order to give it a bit of distance from /O;
- The /two has a small disconnect in its diagonal stroke;
- The /four has a diagonal stroke that terminates vertical above
  the short vertical stroke, the original drawing has the
  diagonal stroke slightly more left at the top;
- On /eight it wasn't really possible to make the lower part
  wider than the upper part;

Major deviations:

- /V is draw with a diagonal lower part and a vertical upper
  part; it was just too ugly to try and follow a diagonal stroke
  all the way;
- Following /V the lowercase /v/w/y were also redrawn with a
  vertical upper part. While this is not so true to the original
  source, it's significantly nicer, and a bit easier to kern.


## REFERENCES

[FX80UM1983] https://bitsavers.org/pdf/epson/printer/P8294014-0_FX80_Users_Manual_1983.pdf

# END
