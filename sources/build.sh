#!/bin/sh

set -e -u

# This script is sources/build.sh and is the
# primary driver for building the fonts in this repo,
# following https://googlefonts.github.io/gf-guide/upstream.html

# Any arguments are past to the romtosvg.py script,
# Useful options are:
# - none at all, in which case the default Dorothy is built
# - --member default,wren which builds the default Dorothy and
#   Dorothy Wren (an alternate persona).

cd ${PWD%font-dorothy*}font-dorothy

rm fonts/ttf/*.ttf *.ttf || true
rm work/*/control-*.csv || true

gasp () {
  # "all on, all sizes" gasp table, as per googlefonts
  # fontbakery check.
  printf '\000\001\000\001\377\377\000\017' > "$1"/gasp
}

prep () {
  # TrueType instructions for enabling smart drop out control;
  # required by the googlefonts fontbakery check (which checks
  # for exactly this sequence).
  printf '\270\001\377\205\260\004\215' > "$1"/prep
}

PYTHONPATH=$PWD/v python sources/romtosvg.py "$@"

mkdir -p fonts/ttf

for C in work/*/control-*.csv
do
  D=$(dirname "$C")
  ttf8 -control "$C" -dir "$D" "$D"/*.svg
  f8name -control "$C" -dir "$D"
  gasp "$D"
  prep "$D"
  assfont -dir "$D"
  mv "${D}".ttf fonts/ttf/
done

# Exit cleanly here in case hb-view not available
hb-view --help > /dev/null || exit 0

# Exit if in CI (to avoid sending PNG files to output)
if [ -n "${CI-}" ]
then
  exit
fi

for f in fonts/ttf/*.ttf
do
  hb-view $f OB5hH7gO
done
