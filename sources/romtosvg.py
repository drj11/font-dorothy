# Convert ROM font to SVG

# https://docs.python.org/3.8/library/argparse.html
import argparse

# https://docs.python.org/3.8/library/importlib.html
import importlib

import os

# https://docs.python.org/3.8/library/pathlib.html
import pathlib

import sys

from dataclasses import dataclass

# 8 digit version (automatically split into YYYY.MMDD and added
# to family branding, and so on).
VERSION = "20250227"

# A little care required here, it is required that it be possible
# to set FAMILY_BRAND to the empty string.
FAMILY_BRAND = f"LAB {VERSION}"
if "DOROTHY_FAMILY_BRAND" in os.environ:
    FAMILY_BRAND = os.environ["DOROTHY_FAMILY_BRAND"]


@dataclass
class Dot:
    radius: ...
    path: ...


def generate(series_module):
    """`series_module` is the Python module to import that
    describes the font and its styles.

    Typically these are dorothy, norma, and wren.
    """

    series = importlib.import_module(series_module)

    # dot radius in its source units
    circle = Dot(
        radius=99,
        path="""
M 99 0 Q 99 41 70 70 41 99 0 99
 -41 99 -70 70 -99 41 -99 0
 -99 -41 -70 -70 -41 -99 0 -99
  41 -99 70 -70 99 -41 99 0 z""",
    )

    for protocol in series.Protocols:
        CreateDorothySource(series.font, protocol, circle, basename=series.basename)


def CreateDorothySource(font, protocol, dot, basename=""):
    """Write the sources required for making a font with ttf8."""

    branchname = protocol.name
    emphasis = "Regular"
    if branchname == "Condensed":
        branchname = "Compressed"
    if branchname == "Elite":
        branchname = "Condensed"
    if branchname == "Elite Emphasized":
        branchname = "Condensed"
        emphasis = "Demi"
    if branchname == "Pica":
        branchname = ""
    if branchname == "Pica Emphasized":
        branchname = ""
        emphasis = "Demi"

    if basename == "":
        basename = "Dorothy Generic Matrix M"
    familyname = " ".join(f"{basename} {branchname}".split())
    familyname = " ".join(f"{familyname} {FAMILY_BRAND}".split())

    postscript_name = "".join(f"{familyname}-{emphasis}".split())

    dir = "work" / pathlib.Path(postscript_name)
    dir.mkdir(parents=True, exist_ok=True)

    blurb = protocol.blurb

    print(protocol)

    emstep = getattr(protocol, "emstep", 9)
    docem = emstep * protocol.vstep

    # Select a UPEM that is a multiple of the empstep.
    upem = [u for u in [1080, 1092] if u % emstep == 0]
    if len(upem) == 0:
        raise Exception(
            "Couldn't find UPEM for emstep %r (which is usually 9 or 13)" % emstep
        )
    upem = upem[0]

    # typo/hhea/win
    # Note vertical range is equivalent to 12 points in document space
    # for the classic 9-pin designs.
    ascender = round(upem * 8.5 / 9)
    descender = round(-upem * 3.5 / 9)
    vendid = "CUBI"
    os2_selection = f"""0000-0000 1{int(emphasis=="Regular")}00-0000"""

    # Radius of the dot in the document units.
    # drj estimates a dot radius of 0.54 gives an approximate
    # stroke width of 0.75 for vertical strokes; and
    # for horizontal strokes at a centre-on-centre spacing of 1.2
    # a width of about 0.61.
    # A docr of 0.5831 points means that the Pica Emphasized
    # style (aka Demi) has no white dot where the 4 circles
    # of a 2x2 arrangement meet in the middle (the circles
    # overlap at that point by a tiny amount).
    # Coincidentally, but conveniently for the TTF encoded
    # scaling in the component, 0.5831 yields a scaling
    # of 5.831 / 99 which is almost exactly 3860*2**-16.
    docr = 0.5831 * protocol.vstep
    dot_scale = docr / dot.radius

    glyphs = protocol.ToDots(font)

    # Currently for the PANOSE calculations,
    # /E and /O are required.
    # And for calculation of Top, /E is also required
    # (derived in the PANOSE calculation).

    Weight = 1

    # For PANOSE Weight (4.X.. .....)
    for glyph in glyphs:
        if glyph.name == "E":
            Top = protocol.vstep * 0.5 + max(y for x, y in glyph.dots)
            # Isolate dots to upper part of vertical stroke.
            dots = [
                (x, y)
                for x, y in glyph.dots
                if x < glyph.advance / 2 and 4 / 7 < y / Top < 6 / 7
            ]
            l, _ = min(dots)
            r, _ = max(dots)
            # The "width" of the stem of /E is the width between the
            # centres + some portion of the dot radius either
            # side; the portion accounting for the fact that
            # PANOSE expects us to use the "average" stroke edge.
            WStemE = r - l + 1.6 * docr
            top = max(y for x, y in glyph.dots)
            bot = min(y for x, y in glyph.dots)
            CapH = top - bot + 1.6 * docr

            WeightRat = CapH / WStemE
            print("PANOSE Weight: %r / %r = %r" % (CapH, WStemE, WeightRat))
            assert 4.5 <= WeightRat < 18
            if WeightRat < 18:
                Weight = 4  # Thin
            if WeightRat < 10:
                Weight = 5  # Book
            if WeightRat < 7.5:
                Weight = 6  # Medium
            if WeightRat < 5.5:
                Weight = 7  # Demi (demi-bold)

    # For PANOSE Aspect (4..X. .....)
    for glyph in glyphs:
        if glyph.name == "O":
            l, _ = min(glyph.dots)
            r, _ = max(glyph.dots)
            # The "width" of the /O is the width between the
            # centres + some portion of the dot radius either
            # side; the portion accounting for the fact that
            # PANOSE expects us to use the "average" stroke edge.
            OWid = r - l + 1.6 * docr
            top = max(y for x, y in glyph.dots)
            bot = min(y for x, y in glyph.dots)
            OTall = top - bot + 1.6 * docr
    ORat = OTall / OWid
    assert 0.92 <= ORat < 2.1
    if ORat < 2.1:
        Aspect = 4
    if ORat < 1.27:
        Aspect = 5

    with open(dir / "control-dorothy.csv", "w") as control:
        control.write(
            f"""\
name,Win/BMP,en-US,1,{familyname}
name,Win/BMP,en-US,2,{emphasis}
name,Win/BMP,en-US,3,{familyname} {emphasis}
name,Win/BMP,en-US,4,{familyname} {emphasis}
name,Win/BMP,en-US,5,Version {VERSION[:4]}.{VERSION[4:]}
name,Win/BMP,en-US,6,{postscript_name}
name,Win/BMP,en-US,9,David R Jones
name,Win/BMP,en-US,10,https://about.cubictype.com/
name,Win/BMP,en-US,11,https://about.cubictype.com/
name,Win/BMP,en-US,19,"-=** {familyname} {emphasis} **=-
{blurb}"
"""
        )
        control.write("docem,{}\n".format(docem))
        control.write("upem,{}\n".format(upem))
        # Panose values estimated somewhat by hand.
        # Some of these values will vary according to various
        # dot parameters.
        # - Position 4 Aspect:
        #   H50 comes out as Condensed 4, and
        #   H60 comes out as Normal 5
        # - Position 5 Contrast, would change with double strike
        #   and other variants.
        control.write(f"os/2,Panose,4 4 {Weight} {Aspect} 2  15 2 2 3 1\n")
        # See https://learn.microsoft.com/en-us/typography/opentype/spec/ibmfc
        control.write("os/2,FamilyClass,{} {}\n".format(8, 10))
        control.write("os/2,VendID,{}\n".format(vendid))
        control.write(f"os/2,Selection,{os2_selection}\n")
        control.write("os/2,TypoAscender,{}\n".format(ascender))
        control.write("os/2,TypoDescender,{}\n".format(descender))
        control.write("os/2,TypoLineGap,0\n".format(upem))

        # cmap
        for index, glyph in enumerate(glyphs):
            for unicode in glyph.unicodes:
                control.write(
                    f"cmap,char,U+{unicode:04X},U+{unicode:04X},glyph,{index}\n"
                )

    with open(dir / "dorothy.svg", "w") as wsvg:
        wsvg.write(
            """
<?xml version="1.0" encoding="UTF-8"?>
<svg xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink" width="10in" height="10in" viewBox="-3600 -3600 3600 3600">
"""
        )
        wsvg.write(
            f"""
<symbol overflow="visible">
<g id="dot" transform="scale({dot_scale})">
<path d="{dot.path}" />
</g>
</symbol>
"""
        )
        for glyph in glyphs:
            wsvg.write("""<g id="%s">\n""" % glyph.name)
            # Dots; reference the component
            for dot in glyph.dots:
                x, y = dot
                y = Top - y
                wsvg.write("""<use x="%r" y="%r" href="#dot" />\n""" % (x, y))
            # Anchors (including for kerning)
            for anchor in glyph.anchors:
                x = anchor.x
                y = Top - anchor.y
                wsvg.write(
                    """<path id="%s" d="M %r %r V %r Z" />\n"""
                    % (anchor.id, x, y, -protocol.vstep)
                )
            # Baseline
            wsvg.write(
                """<path class="baseline" d="M 0 %r H %r Z" />\n"""
                % (Top, glyph.advance)
            )
            wsvg.write("""</g>\n""")
        wsvg.write("""</svg>\n""")


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--members",
        type=str,
        default="dorothy",
        help="comma separated list of family members",
    )

    args = parser.parse_args(argv[1:])

    ms = [m.strip() for m in args.members.split(",")]
    for member in ms:
        # In the past, an indirection table was used, to
        # map to the actual Python module (in a file),
        # but it seems simpler if the Python module is named
        # after the family member.
        generate(member)


if __name__ == "__main__":
    main()
