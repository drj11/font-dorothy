# map.py

"""Functions and classes for mapping from
various glyph representations to, ultimately,
dots suitable for the SVG output.
"""

import itertools
import re

# https://docs.python.org/3.9/library/dataclasses.html
from dataclasses import dataclass

# Local
import agl

###
### Protocols
###


@dataclass
class Protocol:
    """A Protocol converts bitmaps to dots."""

    name: str
    # Horizontal step (in deci-points) from the centre of one
    # pin to the next available strike position. This is the
    # full step, with the understanding that half-step strikes
    # are possible, as long as they do not immediately follow a
    # strike.
    xstep: 9
    # Name String 17, called "Sample Text" in Glyphs.
    blurb: str = "/// Print like it’s 1984 ///"
    # Vertical step (in deci-points) from the centre of one pin to the pin
    # above it. Fixed by the design of the 9-pin print-head.
    vstep: float = 10
    # The number of xsteps for the monospaced advance width.
    advance: float = 6
    # Proportional means that advance widths are derived from
    # the raw glyph.
    proportional: bool = False

    # Emphasized duplicates the dots a half-step to the right.
    # On authentic hardware this is only available in Pica
    # (xstep = 12).
    emphasized: bool = False

    # Used externally; specifies the size of the Em
    # in (v-)steps. UPEM = vstep * emstep.
    emstep: float = 9

    def GlyphToDots(self, glyph):
        """Generally, convert a single glyph from a discrete representation,
        of columns of dots on an abstract (half-) integer grid to
        a concrete geometry representation.
        Returns a fresh glyph instance
        that has a .dots attribute that contains a list
        of (x,y) pairs of the dot-centres.
        The .advance attribute gives the advance width;
        The .anchors attribute has anchors with (x,y) geometry.
        """

        ds = []

        # Dimension in deci-points.
        # Y-coordinate of the nominal bottom of each bitmap,
        # when converting to the x–y grid.
        # For all 9-pin designs this is -2*vstep, but
        # is only apparent when converting to font since
        # actual printing on a 9-pin printer doesn't use a
        # baseline at all.
        y0step = -self.emstep // 5
        y0 = y0step * self.vstep

        # The horizontal step from one pin position to
        # the next potential pin position. A notional half-step.
        halfstep = self.xstep / 2

        def itox(i):
            """Convert discrete grid i to x."""
            # Note the +1 so that the left-most dot (column 0)
            # is positioned with its centre one half-step
            # to the right of the x=0 position.
            return (i+1) * halfstep

        def jtoy(j):
            """Convert discrete grid j to y."""
            return y0 + (j + 0.5) * self.vstep


        for i, col in enumerate(glyph.columns):
            x = itox(i)
            for j, bit in enumerate(col):
                if bit == "#":
                    y = jtoy(j)
                    ds.append((x, y))
                    if self.emphasized:
                        ds.append((x + halfstep, y))

        anchors = []
        for a in glyph.anchors:
            na = Anchor(id=a.id, x=itox(a.i), y=jtoy(a.j))
            anchors.append(na)

        # "Clone" the Glyph into a dict, which
        # will get modified then used to make the
        # fresh Glyph to return.
        d = dict(vars(glyph))

        d["dots"] = ds
        if self.proportional:
            advance = len(glyph.columns) / 2 * self.xstep
        else:
            advance = self.advance * self.xstep
        d["advance"] = advance

        d["anchors"] = anchors

        return Glyph(**d)

    def ToDots(self, font):
        """Convert an entire font"""
        gs = []
        for glyph in font:
            gs.append(self.GlyphToDots(glyph))
        return gs


@dataclass
class Glyph:
    """A glyph and its bits."""

    columns: list
    infos: None
    unicodes: list
    anchors: list
    name: ... = None
    dots: ... = None
    advance: ... = None


@dataclass
class Anchor:
    """An anchor in a glyph."""

    id: str
    i: int = None
    j: int = None
    x: float = None
    y: float = None


def RawToGlyph(s, index, rows=None, lsbadj=0):
    """Convert a glyph in raw bit form to a Glyph instance.
    The returned Glyph has a name (which will be generated from
    its ASCII position only if it is in range 0x20 to 0x7f);
    it will have a list of unicodes, which may be empty.
    The columns attribute will be a sequence of columns,
    each column starting at the _bottom_ (least y in the usual
    coordinate system for font drawings).

    *rows* is the number of body rows that a glyph should have;
    if supplied, checks that the glyph has the right number of
    rows.

    *lsbadj* is used to adjust the left side-bearing.
    Principally for the Espon FX-80 design,
    where most of the dot matrix glyphs are 5 dots wide on a "body"
    6 dots wide, but start in the left-hand column (for example, /H).
    Operation: if the number of supplied columns in the glyph is < lsbadj,
    then a single column of space is added on the left
    (so the default lsbjad=0 disables this adjustment entirely).
    For the Epson design, this is used to centre the 5-dot glyphs.
    """

    raw_rows = s.split("\n")
    m = []
    # Split into blocks for bits (graphics is True) and
    # rows for metadata (graphics is False).
    for graphics, block in itertools.groupby(raw_rows, lambda r: r[0] in "-#"):
        if not graphics:
            # metadata
            m = list(block)
        if graphics:
            # row data
            block = list(block)
            # Make lsb adjustment, if needed.
            w = min(len(r) for r in block)
            if w < lsbadj:
                block = ["-" + r for r in block]
            bit_rows = block

    name = None
    names = []
    unicodes = []
    for ss in m:
        for s in ss.split(";"):
            s = s.strip()
            if s.startswith("/"):
                names.append(s[1:])
            if s.startswith("U+"):
                unicodes.append(int(s[2:], 16))

    if not unicodes and not names:
        assert 32 <= index < 128
        unicodes.append(index)

    if len(names) > 1:
        print("A glyph has only one name; this one has: %r\n" % names, file=sys.stderr)
    if names:
        name = names[0]
    else:
        unicode = unicodes[0]
        name = agl.uton(unicode)

    # The distinction between body rows and extra rows was
    # made specifically for tgl31034: it allows extra extension rows
    # above the body rows, and is used for /Udieresis and so on.
    # Extension rows are marked with a trailing +.
    body_rows = [row for row in bit_rows if not row.endswith("+")]
    assert rows == len(body_rows), """name: %r; required rows: %d; got rows: %d""" % (
        name,
        rows,
        len(body_rows),
    )

    # Implicitly, each rows must have at least as many columns as the
    # bottom-most row, and any beyond that length is ignored.
    bit_rows = list(reversed(bit_rows))
    cols = []
    for i in range(len(bit_rows[0])):
        col = [row[i] for row in bit_rows]
        cols.append(col)

    # Anchors
    anchors = []
    for j, row in enumerate(bit_rows):
        if "." not in row:
            continue
        # "." indicates anchor present
        match = re.search(r"\.([^-+#]+)", row)
        i = match.start()
        id = match[1]
        anchors.append(Anchor(id=id, i=i, j=j))

    g = Glyph(columns=cols, infos=m, name=name, unicodes=unicodes, anchors=anchors)
    return g
