#!/bin/sh

# Build the images used for samples.

set -e -u

# What a shellful to get the shortest filename matching
# *-Regular.tff
F=$(find fonts -name '*-Regular.ttf' |
  awk '{print length, $0}' | sort -n |
  awk '{print $2}' | sed q)

mkdir -p image

# From text/
for T in text/*.txt
do
    hb-view --font-size 144 --margin 9 "$F" "$(cat "$T")" > image/"$(basename "$T" .txt)".png
done


# Plaques
plak --order code --font-size=216 --margin=12 "$F" > image/$(basename "$F" .ttf)-plaque.png
